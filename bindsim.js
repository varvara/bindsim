// Constants
var ENDPOINT = "api/sim"; // Simulator endpoint url
var PLOT = "plot"; // ID of plot container
var COLOR_MOLEFRAC_HOST = "rgba(8, 87, 255, 0.4)";
var COLOR_MOLEFRAC_COMPLEX = "rgba(126, 198, 254, 0.4)";
var COLOR_ISOTHERM = "rgba(255, 73, 0, 1)";

var bindsim = {
 
    on_ready: function() {
        /**
         * Initialisation function, called on document ready
         */

        // Initialise empty chart
        bindsim.chart = bindsim.plot_setup(PLOT,
                                           "", 
                                           ""
                                           );

        $("#button-plot").on("click", bindsim.plot);

        // Trigger replot on Enter
        $("#params :input").on("keyup", function(event) {
            //if(event.keyCode == 13) {
                bindsim.plot(event);
            //}
        });
        
        // Populate plot on initialisation with default values
        $("#button-plot").click();
    },

    plot: function(event) {
        /**
         * Parses control form input, passes to backend and plots result. 
         * Called on plot button click.
         */

        // Top-level function, do everything!

        // Parse form into request json for bindsim api
        request = bindsim.params_to_json(bindsim.parse_fail);
        console.log("Parsed request json:");
        console.log(request);

        // Call bindsim with parsed request
        bindsim.bindsim_call(request, 
                             ENDPOINT,
                             bindsim.plot_update, 
                             bindsim.backend_fail);
    },

    //
    // Main functions
    // 
    plot_update: function(points, request) {
        /**
         * Redraws plot with new data
         * 
         * @param {Object} points - New x, y points to plot
         * @param {array} points.isotherm - Simulated points, format: [[x,y],..n]
         * @param {array} points.molefrac_host - As above
         * @param {array} points.molefrac_complex - As above
         */

        console.log("plot_update: Received returned points");
        console.log(points);
        
        // Set appropriate extremes
        shift_min = parseFloat($("#shifth").val());
        shift_max = parseFloat($("#shifthg").val());
        bindsim.chart.get("axis-isotherm").setExtremes(shift_min, shift_max);

        // Set ticks (workaround for setExtremes bug)
        // TODO: this breaks everything, why?
/*
        bindsim.chart.get("axis-isotherm").update({
            tickPositioner: function() {
                var positions = [];
                var tick = this.dataMin;
                var increment = (this.dataMax - this.dataMin)/6;

                for (tick; tick - increment <= this.dataMax; tick += increment) {
                    positions.push(tick);
                }
                return positions;
            }
        });
*/

        console.log("Isotherm y axis extremes (after set):");
        console.log(bindsim.chart.yAxis[0].getExtremes());

        // Plot new data
        bindsim.chart.get("series-molefrac-host")
            .setData(points["molefrac_host"], false);
        bindsim.chart.get("series-molefrac-complex")
            .setData(points["molefrac_complex"], false);
        bindsim.chart.get("series-isotherm")
            .setData(points["isotherm"], false);
        bindsim.chart.redraw();
    },

    bindsim_call: function(request, endpoint, on_done, on_fail) {
        /**
         * Call bindsim backend and execute specified callback on completion
         *
         * @param {Object} request - Appropriately structured request object for bindsim backend API
         * @param {callback} on_done - Callback to execute on call success, passed backend response and request object
         * @param {callback} on_faile - Callback to execute on call failure, passed backend response and request object
         */
        
        var jqxhr = $.ajax({
            url: endpoint,
            type: "POST",
            data: JSON.stringify(request),
            contentType: "application/json; charset=utf-8",
            dataType: "json"
        });

        jqxhr.done(function(data) {
            on_done(data, request);
        });

        jqxhr.fail(function(data) {
            on_fail(data, request);
        });
    },

    params_to_json: function(on_fail) {
        /**
         * Parses control form into a JS Object for passing to bindsim backend
         *
         * @param {callback} on_fail - Callback to execute on form parsing failure
         *
         * @returns {Object} - Parsed form contents in format expected by bindsim backend
         */

        // Don't serialize empty fields
        var $params = $("#params :input").filter(
                function(index, element) {
                    return $(element).val() != "";
                });

        var params = {};
        $params.serializeArray().map(function(x){params[x.name] = x.value;}); 
        
        // Ensure all required fields are filled
        if (bindsim.params_check(params) === false) {
            on_fail();
            return null;
        } else {
            return params;
        }
    },



    // 
    // Failure functions
    //
    parse_fail: function() {
        // Form parsing failure
        console.log("ERROR: Failed to parse form data, check your input")
    },

    backend_fail: function(data, request) {
        // Backend failure
        console.log("ERROR: Backend error")
    },



    // 
    // Convenience functions
    //
    params_check: function(params) {
        /**
         * Checks parameter object (to be passed to bindsim backend) for errors
         *
         * @param {Object} params - Parsed form contents in format expected by bindsim backend
         *
         * @returns {Boolean} - true on success, false on failure
         */
        return true;
    },

    

    //
    // Highcharts
    // 
    plot_setup: function(container, title, subtitle) {
        /**
         * Initialise a Highcharts chart in the supplied container
         *
         * @param {string} container - ID of container to render chart to
         * @param {string} title - Chart title
         * @param {string} subtitle - Chart subtitle
         *
         * @returns {Object} - Highcharts chart object
         */

        var chart = new Highcharts.Chart({
            chart: {
                renderTo: container,
            },
            title: {
                text: title,
            },
            subtitle: {
                text: subtitle,
            },
            xAxis: {
                title: {
                    text: "Equivalent total [G]\u2080/[H]\u2080"
                },
                labels: {
                    format: "{value}"
                }
            },
            yAxis: [{ // Primary y axis
                id: "axis-isotherm",
                title: {
                    text: "\u0394\u03B4 (ppm or Hz)"
                },
                labels: {
                    format: "{value} (ppm or Hz)"
                },
                minPadding: 0,
                maxPadding: 0,
                startOnTick: false,
                endOnTick: false
            }, { // Secondary y axis
                id: "axis-molefrac",
                title: {
                    text: "Host (H) vs. Complex (HG) Molefraction"
                },
                labels: {
                    format: "{value}"
                },
                opposite: true,
                min: 0,
                max: 1,
                minPadding: 0,
                maxPadding: 0,
                startOnTick: false,
                endOnTick: false
            }],
            tooltip: {
                shared: true
            },
            legend: {
                layout: 'vertical',
                floating: true,
                align: 'left',
                verticalAlign: 'top',
                x: 100,
                borderWidth: 0
            },
            series: [{
                id: "series-molefrac-host",
                name: "Host (H) molefraction",
                type: "area",
                yAxis: "axis-molefrac",
                color: COLOR_MOLEFRAC_HOST
            }, {
                id: "series-molefrac-complex",
                name: "Complex (HG) molefraction",
                type: "area",
                yAxis: "axis-molefrac",
                color: COLOR_MOLEFRAC_COMPLEX
            }, {
                id: "series-isotherm",
                name: "Isotherm",
                type: "line",
                yAxis: "axis-isotherm",
                tooltip: {
                    valueSuffix: " (ppm or Hz)"
                },
                lineWidth: 5,
                color: COLOR_ISOTHERM
            }]
        });

        return chart;
    }

};
 
$(document).ready(bindsim.on_ready);
