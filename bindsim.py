from __future__ import division
from __future__ import print_function

from flask import Flask, request, jsonify
from flask.ext.cors import CORS

import numpy as np
from math import sqrt

import sys

# Flask setup
app = Flask(__name__)
cors = CORS(app)
app.debug = True

# Constants
N = 100 # Number of steps in simulation



#
# API routing and responses
#

@app.route("/sim", methods=['POST'])
def sim():
    """
    Runs requested simulation and returns simulated isotherm and molefractions
    as a function of equivalent [G]0/[H]0 fraction.

    Query:
        Format: json
        {
            bindconst    : float, Binding constant Ka
            host0_init   : float, Initial [H]0
            guesteq_final: float, Max equiv. [G]0/[H]0
            shifth       : float, Free host NMR resonance
            shifthg      : float  Host-Guest complex NMR resonance
        }

    Response:
        Format: json
        {
            molefrac_host   : array of [x, y] points for plotting
            molefrac_complex: array of [x, y] points for plotting
            isotherm        : array of [x, y] points for plotting
        }
    """

    query = request.get_json(force=True)
    # Convert any numerical values in query dict to python int/floats
    queryparsed = { k:str_to_num(v) for k, v in query.items() }

    # Run simulator
    #guesteq, shiftdelta, hostfrac, complexfrac = sim_nmr_1to1(**queryparsed)
    guesteq, shiftdelta, hostfrac, complexfrac, mf_hg2 = sim_nmr_1to2()

    # Build response dict
    responsedict = {
            "molefrac_host": [[x, y] for x, y in zip(guesteq, hostfrac)],
            "molefrac_complex": [[x, y] for x, y in zip(guesteq, complexfrac)],
            "isotherm": [[x, y] for x, y in zip(guesteq, shiftdelta)],
            }

    response = jsonify(**responsedict)
    return response



#
# Simulator functions
#

def sim_nmr_1to1(bindconst=1000,
                 host0_init=0.001,
                 guesteq_init=0,
                 guesteq_final=20,
                 shifth=8,
                 shifthg=9,
                 num=N):
    """
    NMR 1:1 binding constant simulator

    Args:
        bindconst    : float  Binding constant Ka
        host0_init   : float  Initial [H]0
        guesteq_init : float  Min equiv. [G]0/[H]0
        guesteq_final: float  Max equiv. [G]0/[H]0
        shifth       : float  Free host NMR resonance
        shifthg      : float  Host-Guest complex NMR resonance
        num          : int    Number of steps to evaluate at

    Returns:
        (guesteq,     : tuple, length 4 of arrays of length num
         shiftdelta,    Simulated isotherm and molefraction curves
         hostfrac,
         complexfrac)

    Raises:
        [none]
    """

    # Initialise x array
    guesteq = np.linspace(guesteq_init, guesteq_final, num)

    # Initialise y arrays
    s = guesteq.shape
    shiftdelta = np.zeros(s)       # Change in chemical shift delta (ppm or Hz)
    hostfrac = np.zeros(s)         # H free host molefraction
    complexfrac = np.zeros(s)      # HG complex molefraction

    for i, geq in enumerate(guesteq):
        # For convenience
        h0 = host0_init
        g0 = geq * h0
        ka = bindconst

        dh = shifth
        dhg = shifthg

        # Calculate host and complex molefractions
        cfrac = (h0 + (1/ka) + g0) - sqrt((h0+(1/ka)+g0)**2 - 4*h0*g0)
        cfrac *= 0.5
        cfrac /= h0

        hfrac = 1 - cfrac

        # Calculate delta shift
        delta = dh + ((dhg - dh)*cfrac)

        shiftdelta[i] = delta
        hostfrac[i] = hfrac
        complexfrac[i] = cfrac

    return guesteq, shiftdelta, hostfrac, complexfrac

def sim_nmr_1to2(k1=10000,
                 k2=1000,
                 h0_init=0.001,
                 g0h0_init=0,
                 g0h0_final=20,
                 dh=8,
                 dhg=7,
                 dhg2=9,
                 num=N):
    """
    NMR 1:2 binding constant simulator

    Args:
        k1         : float  Binding constant K1
        k2         : float  Binding constant K2
        h0_init    : float  Initial [H]0
        g0h0_init  : float  Min equiv. [G]0/[H]0
        g0h0_final : float  Max equiv. [G]0/[H]0
        dh         : float  Free host NMR resonance
        dhg        : float  Host-Guest complex NMR resonance
        dhg2       : float  Host-Guest2 complex NMR resonance
        num        : int    Number of steps to evaluate at

    Returns:
        (g0h0,     : tuple, length 5 of arrays of length num
         dd,         Simulated isotherm and molefraction curves
         mf_h,
         mf_hg,
         mf_hg2)

    Raises:
        [none]
    """

    # Convert all input to float64s
    k1         = np.float64(k1)
    k2         = np.float64(k2)
    h0_init    = np.float64(h0_init)
    g0h0_init  = np.float64(g0h0_init)
    g0h0_final = np.float64(g0h0_final)
    dh         = np.float64(dh)
    dhg        = np.float64(dhg)
    dhg2       = np.float64(dhg2)

    dtype = np.dtype('f8')

    # Initialise x array
    g0h0 = np.linspace(g0h0_init, g0h0_final, num, dtype=dtype)

    # Initialise y arrays
    shape = g0h0.shape
    dd = np.zeros(shape, dtype)       # Change in chemical shift delta (ppm or Hz)
    mf_h = np.zeros(shape, dtype)     # H free host molefraction
    mf_hg = np.zeros(shape, dtype)    # HG complex molefraction
    mf_hg2 = np.zeros(shape, dtype)   # HG2 complex molefraction

    for i, geq in enumerate(g0h0):
        # For convenience
        h0 = h0_init
        g0 = geq * h0

        # Calculate reduced cubic coefficients (for cubic in [G])
        a = 1
        b = 2*h0 - g0 + 1/k2;
        c = h0/k2 - g0/k2 + 1/(k1*k2);
        d = (-g0)/(k1*k2);

        p = np.array([a, b, c, d], dtype=dtype)

        # Find cubic roots (solve for [G])
        roots = np.roots(p).astype(dtype)

        print("DEBUG", file=sys.stderr)
        print("roots:", roots, file=sys.stderr)
        print("len(roots):", len(roots), file=sys.stderr)

        # Find smallest real positive root:
        select = np.all([np.imag(roots) == 0, np.real(roots) >= 0], axis=0)
        print("select:", select)
        print("len(select):", len(select))
        print("roots[select]:", roots[select])
        g = roots[select].min()
        print("+ve: ", g, file=sys.stderr)
        print("+ve dtype: ", g.dtype, file=sys.stderr)
        g = float(np.real(g))
        print("+ve after cast: ", g, file=sys.stderr)

        mf_hg[i]  = (g*k1)/(1 + k1*g + k2*k1*(g**2))
        mf_hg2[i] = (k2*k1*(g**2))/(1 + k1*g + k2*k1*(g**2))
        mf_h[i]   = 1 - mf_hg[i] - mf_hg2[i]
        dd[i]     = dh*mf_h[i] + dhg*mf_hg[i] + dhg2*mf_hg2[i]

    return g0h0, dd, mf_h, mf_hg, mf_hg2



#
# Convenience functions
#

def str_to_num(s):
    # Return float value of string if the string is parsable into float
    if s.isdigit():
        return int(s)
    else:
        try:
            return float(s)
        except ValueError:
            return s

#
# Debug
#

if __name__ == '__main__':
    app.run()
